public class Solution {
    static int start=0;
    static int length=0;
    
    public static void main(String args[]) {
        System.out.println(palindrome("abcdddcfe"));
    }
    
    static String longestPalindrome(String s) {
        if(s.length() < 2) return s;
        
        for(int i=0; i < s.length()-1; i++) {
            helper(s, i, i);
            helper(s, i, i+1);
        }
        
        return s.substring(start, start + length);
    }
    
    static void helper(String s, int i, int j) {
        while(i >= 0 && j < s.length() && s.charAt(i) == s.charAt(j)) {
            i--;
            j++;
        }
        
        if(j - i - 1 > length) {
            length = j - i - 1;
            start = i + 1;
        }
    }
}